package book;

import com.company.LinkedQueue;

public class iteratorBFS{
    public Iterator<T> iteratorBFS(int startIndex) {
        int currentVertex;
        LinkedQueue<Integer> traversalQueue = new LinkedQueue<Integer>();
        ArrayIterator<T> iter = new ArrayIterator<T>();
        if(!indexIsValid(startIndex))
            return iter;
        boolean[] visited = new boolean[numVertices];
        for(int vertexIndex = 0;vertexIndex < numVertices;vertexIndex++)
            visited[vertexIndex]=false;
        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;
        while ( !traversalQueue.isEmpty())
        {    currentVertex = traversalQueue.dequeue( );
            iter.add (vertices[currentVertex]);
            for (int vertexIndex = 0; vertexIndex < numVertices );vertexIndex++ )
            if (adjMatrix [ currentVertex ] [vertexIndex] &&!visited[ vertexIndex]){
                traversalQueue.enqueue(vertexIndex) ;
                visited[vertexIndex] = true;
            }
        }
        return iter ;
    }
    public Iterator<T> iteratorDFS( int startIndex){
        int currentVertex;
        LinkedStack<Integer> traversalstack =
                new LinkedStack<Integer>( ) ;
        ArrayIterator<T> iter = new ArrayIterator<T>() ;
        boolean[ ] visited = new boolean [ numVertices ] ;
        boolean found;
        if (! indexIsvalid(startIndex))
            return iter;
        for (int vertexIdx = 0; vertexIdx < numVertices ;
             vertexIdx++)
            vis ited[vertexIdx] = false;
        traversalStack. push(startIndex) ;
        iter.add (vertices[startIndex]);
        visited[ startIndex] = true;
        while ( ItraversalStack. isEmpty()) {
            currentVertex = traversalStack.peek();
            found = false; ，
            for (int vertexIdx = 0; vertexIdx < numVertices && !found;
                 vertexIdx++)
                if (adjMatrix[currentVertex][vertexIdx] && !visited[vertexIdx]) {
                    traversalStack.push(vertexIdx);
                    iter.add(vertices[vertexIdx]);
                    visited[vertexIdx] = true;
                    found = true;
                }
        }
        if(!found&&！traversalStack.isEmpty())
            traversalStack.pop();
    }
return iter;



}
