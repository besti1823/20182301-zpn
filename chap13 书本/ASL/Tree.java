package com.company;




import java.util.Scanner;
import java.util.StringTokenizer;

public class Tree {

    private static Node root;
    private Node temp;
    private String str = "";


    public static void main(String[] args) {
        String st = "19 14 23 1 68 20 84 27 55 11 10 79";
        StringTokenizer tokenizer = new StringTokenizer(st);
        int[] number = new int[tokenizer.countTokens()];
        int i = 0;
        while(tokenizer.hasMoreTokens()){
            number[i] = Integer.parseInt(tokenizer.nextToken());
            i++;
        }
        Tree tree = new Tree();
        for(int m=0;m<number.length;m++){
            tree.add(number[m]);
        }
        tree.print();
        System.out.println("请输入你想查找的数据： ");
        Scanner scan = new Scanner(System.in);
        int cha = scan.nextInt();
        scan.nextLine();
        Node n=tree.search(root,cha);
        if(n!=null)
        System.out.println("二叉查找树查找到的结果为： "+n.getData());
        else
            System.out.println("找不到！");

    }




        public Tree() {
            root = null;
        }

        //向二叉树插入元素
        public void add(int data1) {
            if (this.root == null) {
                root = new Node(data1);
                temp = root;//存下第一个节点
            } else {
                addW(root,data1);
            }

        }

        //判定插入元素的位置
        public void addW(Node x,int data1) {
            //当数据大于等于节点时，放右子树
            if (data1 >= x.getData()) {
                if (x.getRight() == null) {
                    x.setRight(new Node(data1));
                } else {
                    addW(x.getRight(),data1);
                }
            }
            //数据小于节点时，放左子树
            else {
                if (x.getLeft() == null) {
                    x.setLeft(new Node(data1));
                } else {
                    this.addW(x.getLeft(),data1);
                }
            }
        }

        public void print() {
            root.print();
            System.out.println();
        }

        //在二叉树中寻找元素
        public Node search(Node x,int data2) {
            if(x==null)
                return null;
            if(data2==x.getData()){
                return x;
            }
            else if(data2<x.getData()){
                return search(x.getLeft(),data2);
            }
            else {
                return search(x.getRight(),data2);
            }
        }

        public Node getRoot() {
            return root;
        }

}
