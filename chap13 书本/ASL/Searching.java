package com.company;



public class Searching {

    public static int linearSearch(int[] data, int target) {
        int result = 0;
        int index=0;
        while(result == 0&&index<data.length){
            if(data[index]==target)
                result = data[index];
            index++;
        }
        return result;
    }

    public static int binarySearch (int[] data,
                                    int target){
        int result = 0;
        int first = 0,last = data.length-1,mid;

        while(result == 0&&first <=last){
            mid=(first + last )/2;
            if(data[mid] == target)
                result = data[mid];
            else
            if(data[mid] > target)
                last = mid-1;
            else
                first = mid+1;
        }
        return  result;
    }



    public static int[] addhashlinear(int[] data){
        int[] result=new int[13];
        for(int i=0;i<data.length;i++){
             int m=data[i]%13;
             while(result[m]!=0)
                 m++;
             result[m]=data[i];
        }
        return result;
    }

    public static int hashlinear(int[] data,int target){
        int m = target%13;
        while(data[m]!=target&&m<data.length-1){
            m++;
        }
        return m;
    }

    public static int[][] addhasgIP(int[] data){
        int[][] result=new int[11][100];
        int n=0;
        for(int i=0;i<data.length;i++){
            int m=data[i]%11;
            while(result[m][n]!=0)
                n++;
            result[m][n]=data[i];
        }
        return result;

    }

    public static int hashIP(int[][] data,int target){
        int n=0;
        int m = target%11;
        while(data[m][n]!=target&&m<data.length-1){
            n++;
        }
        return data[m][n];

    }



}
