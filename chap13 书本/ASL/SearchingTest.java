package com.company;

import java.util.Scanner;
import java.util.StringTokenizer;

import static com.company.Searching.*;

public class SearchingTest {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        String str = "19 14 23 1 68 20 84 27 55 11 10 79";
        StringTokenizer tokenizer = new StringTokenizer(str);
        int[] number = new int[tokenizer.countTokens()];
        int i = 0;
        while(tokenizer.hasMoreTokens()){
            number[i] = Integer.parseInt(tokenizer.nextToken());
            i++;
        }
        int[] number1 ;
        number1=PX(number);
        System.out.println("请输入你要查找的对象(19 14 23 1 68 20 84 27 55 11 10 79)： ");
        int cha = scan.nextInt();
        int a = linearSearch(number,cha);
        System.out.println("线形查找到的对象是： "+a);
        int b = binarySearch(number1,cha);
        System.out.println("二分查找到的对象是： "+b);
        int[] number2=addhashlinear(number1);
        int c = hashlinear(number2,cha);
        System.out.println("散列线性查找到的对象是： "+number2[c]);
        int[][] number3=addhasgIP(number);
        int d = hashIP(number3,cha);
        System.out.println("散列链地址查找到的对象是： "+d);


    }
    public static int[] PX(int[] arr){
        int temp;

        for(int i=0;i<arr.length;i++) {
            int tem = i;
            for(int j=i;j<arr.length;j++) {
                if(arr[j] < arr[tem]) {
                    tem = j;
                }
            }
            int temp1 = arr[i];
            arr[i] = arr[tem];
            arr[tem] = temp1;
        }

        return arr;
    }
}
