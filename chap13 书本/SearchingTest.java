package com.company;

import java.util.Scanner;
import java.util.StringTokenizer;

import static com.company.Searching.*;

public class SearchingTest {
    public static void main(String[] args) {
        Scanner scan =new Scanner(System.in);
        System.out.println("请输入你想输入数字的个数：  ");
        int size = scan.nextInt();
        System.out.println("请输入一串数字：  ");
        String str1 = scan.nextLine();
        String str = scan.nextLine();
        StringTokenizer tokenizer = new StringTokenizer(str);
        int[] number = new int[tokenizer.countTokens()];
        int i = 0;
        while(tokenizer.hasMoreTokens()){
            number[i] = Integer.parseInt(tokenizer.nextToken());
            i++;
        }
        int[] abc ;
        abc=PX(number);
        System.out.println("请输入你要查找的对象： ");
        int cha = scan.nextInt();
        int a = linearSearch(number,cha);
        System.out.println("线形查找到的对象是： "+a);
        int b = binarySearch(number,cha);
        System.out.println("二分查找到的对象是： "+b);
        boolean c = binary2Search(number,abc[0],abc[size-1],cha);
        if(c==true)
            System.out.println("递归查找已经找到");
        else
            System.out.println("递归查找未被找到");

    }
    public static int[] PX(int[] arr){
        int temp;
        System.out.println(arr.length);
        for(int i=0;i<arr.length;i++) {
            int tem = i;
            for(int j=i;j<arr.length;j++) {
                if(arr[j] < arr[tem]) {
                    tem = j;
                }
            }
            int temp1 = arr[i];
            arr[i] = arr[tem];
            arr[tem] = temp1;
        }
        for(int i=0;i<arr.length;i++)
        System.out.println(arr[i]);
        return arr;
    }
}
