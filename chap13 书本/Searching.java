package com.company;



public class Searching {

    public static int linearSearch(int[] data, int target) {
        int result = 0;
        int index=0;
        while(result == 0&&index<data.length){
            if(data[index]==target)
                result = data[index];
            index++;
        }
        return result;
    }

    public static int binarySearch (int[] data,
                                    int target){
        int result = 0;
        int first = 0,last = data.length-1,mid;

        while(result == 0&&first <=last){
            mid=(first + last )/2;
            if(data[mid] == target)
                result = data[mid];
            else
            if(data[mid] > target)
                last = mid-1;
            else
                first = mid+1;
        }
        return  result;
    }

    public static boolean binary2Search(int[] data, int min, int max, int target){
        boolean found =false;
        int midpoint  =(min+max)/2;
        if(data[midpoint]==target)
            found = true;
        else if (data[midpoint]>target){
            if(min<midpoint-1)
                found = binary2Search(data,min,midpoint-1,target);
        }
        else {if(midpoint + 1<max)
            found = binary2Search(data,midpoint+1,max,target);}
        return found;
    }

}
