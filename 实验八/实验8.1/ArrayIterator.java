package com.company;

import java.util.ArrayList;
//使用iterator遍历集合的同时对集合进行修改就会出现java.util.ConcurrentModificationException异常
import java.util.ConcurrentModificationException;
import java.util.Iterator;

public class ArrayIterator<T> extends ArrayList<T> implements Iterator<T> {

    int iteratorModCount;
    int current;
    public ArrayIterator()
    {
        iteratorModCount = modCount;
        current = 0;

    }
    public boolean hasNext() throws ConcurrentModificationException
    {
        return super.iterator().hasNext();
    }
    public T next() throws ConcurrentModificationException
    {
        return super.iterator().next();
    }

    //java.util.ArrayList重新了这些方法而Arrays的内部类ArrayList没有重新，所以会抛出异常
    //解决方法：List<String> list = Arrays.asList(array);
    //          List arrList = new ArrayList(list);
    public void remove() throws UnsupportedOperationException
    {
        throw new UnsupportedOperationException();
    }
}