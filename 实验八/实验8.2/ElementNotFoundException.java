package com.company;

public class ElementNotFoundException extends Throwable {
    public ElementNotFoundException(String collection) {
        super (collection + "没有找到特定元素。");

    }
}
