package com.company;



import com.company.LinkedBinaryTree;

import java.util.Scanner;

public class InferenceTree {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String inOrder = scanner.nextLine();   // 中序
        String preOrder = scanner.nextLine();    //  先序

        char[] in = inOrder.toCharArray();
        char[] pre = preOrder.toCharArray();
//        System.out.println(pre[0]);

        LinkedBinaryTree tree = returnTree(in,pre);   // 递归方法调用
        System.out.println(tree.toString());
//
//        int x = 0;
//        while (in[x] != pre[0])
//        {
//            x++;
//        }
//
//        char[] inLeft = new char[x];
//        char[] preLeft = new char[x];
//        char[] inRight = new char[in.length - x - 1];
//        char[] preRight = new char[pre.length - x - 1];


    }

    public static LinkedBinaryTree returnTree(char[] in, char[] pre)
    {
        LinkedBinaryTree tree;
        if(pre.length == 0 || in.length == 0 || pre.length != in.length){ // 终止递归的条件
            tree =  new LinkedBinaryTree();
        }
        else {
            int x = 0;
            while (in[x] != pre[0]) {   // 找到根结点
                x++;
            }

            char[] inLeft = new char[x];       // 根结点的左边为左子树，创建新的数组
            char[] preLeft = new char[x];
            char[] inRight = new char[in.length - x - 1];       // 根结点的右边为右子树，创建新的数组
            char[] preRight = new char[pre.length - x - 1];

            for (int y = 0; y < in.length; y++) {   // 把原数组的数存入新的数组当中
                if (y < x) {
                    inLeft[y] = in[y];
                    preLeft[y] = pre[y + 1];
                } else if (y > x) {
                    inRight[y - x - 1] = in[y];
                    preRight[y - x - 1] = pre[y];
                }
            }
            LinkedBinaryTree left = returnTree(inLeft, preLeft);   // 左子树递归调用
            LinkedBinaryTree right = returnTree(inRight, preRight); // 右子树递归调用
            tree = new LinkedBinaryTree(pre[0], left,right);
        }
        return tree;//HDIBEMJNAFCKGL;ABDHIEJMNCFGKL
    }
}
