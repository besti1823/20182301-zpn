package com.company;

public class Employee extends  StaffMember {
    protected String socialSecurityNumber;
    protected int dayy;
    public Employee(String eName,String eAddtress,String ePhone,String socSecNumber,int day)
    {
        super (eName,eAddtress,ePhone);
        socialSecurityNumber= socSecNumber;
        dayy = day;
    }

    @Override
    public String toString() {
        String result = super.toString();
              result+=  "\n social  Security  Number='" + socialSecurityNumber ;
              return result;
    }

    @Override
    public int holiday() {
        return dayy;
    }


}
