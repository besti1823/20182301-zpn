package com.company;

import java.io.IOException;

public class SecretTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Secret hush=new Secret("Wil Wheaton is my hero!");
        System.out.println(hush);
        hush.encrypt();
        System.out.println(hush);
        hush.decrypt();
        System.out.println(hush);

        Password hash=new Password("Wil Wheaton is my hero!");
        System.out.println(hash);
        hash.encrypt();
        System.out.println(hash);
        hash.decrypt();
        System.out.println(hash);
    }
}
