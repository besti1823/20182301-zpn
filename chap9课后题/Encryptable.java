package com.company;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface Encryptable {
    public void encrypt() throws IOException, ClassNotFoundException;
    public String decrypt() throws IOException, ClassNotFoundException;
}
