package com.company;

public class Hourly extends Employee {
    private  int hoursWorked;

    public Hourly(String eName, String eAddtress, String ePhone, String socSecNumber, int day) {
        super(eName, eAddtress, ePhone, socSecNumber, day);
        hoursWorked = 0;
    }
    public void addHours(int moreHours)
    {
        hoursWorked += moreHours;
    }
    public int holiday()
    {
        return 0;
    }
    public String toString() {
        String result = super.toString();
        result+=  "\n Current hours='" + hoursWorked ;
        return result;
    }
}
