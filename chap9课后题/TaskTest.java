package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class TaskTest {
    public static void main(String[] args) {
        Task[] list=new Task[4];
        int[] a=new int[4];
        List<Task> tasklist=new ArrayList<>();

        Task task=new Task("");
        list[0]=new Task("Java1");
        list[1]=new Task("Java2");
        list[2]=new Task("Java3");
        list[3]=new Task("Java4");

        for(int i=0;i<4;i++){
            tasklist.add(list[i]);
        }

        Scanner in=new Scanner(System.in);
        System.out.println("请输入一组优先级：(数字越大优先级越高）");
        for(int i=0;i<4;i++){
            a[i]=in.nextInt();//接受优先级
        }

        task.set(list,a);
        for(Task c:list){
            System.out.println(c.getName()+",优先级:"+c.getPriority());
        }
        //尝试列表排序
        //   System.out.println("排序后：");
        // Collections.sort(tasklist);
        //for(Task c:tasklist){
        //    System.out.println(c.getName()+",优先级:"+c.getPriority());
        // }

        Arrays.sort(list);
        for(Task c:list){
            System.out.println(c.getName()+",优先级:"+c.getPriority());
        }

    }
}
