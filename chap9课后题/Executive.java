package com.company;

public class Executive extends Employee{
    private double bonus;
    public Executive(String eName,String eAddtress,String ePhone,String socSecNumber,int day)
    {
        super (eName,eAddtress,ePhone,socSecNumber,day );
        bonus = 0;
    }
    public void awardBonus(double execBonus)
    {
        bonus = execBonus;
    }
    public int holiday(){
        return 2;
    }

}
