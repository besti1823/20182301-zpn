package com.company;


public class Task implements Priority,Comparable<Task> {
    private int priority;
    private String name;

    public Task(String name) {
        this.name = name;
    }

    @Override
    public void setPriority(int n) {
        this.priority=n;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    public void set(Task[] t,int[] n){//建立Task【】，输入要做的事
        for(int i=0;i<t.length;i++){
            t[i].setPriority(n[i]);
        }
    }

    public String getName() {
        return this.name;
    }

    @Override
    public int compareTo(Task c) {//比较优先级
        if(this.priority>c.priority)
            return 1;
        else if(this.priority<c.priority)
            return -1;
        else
            return 0;
    }

}