package com.company;

import java.util.Scanner;

public class CreatingExceptions {
    public static void main(String[] args) throws OutOfRangeException {
        final int MIN = 25,MAX=40;
        Scanner in =new Scanner(System.in);
        OutOfRangeException e=new OutOfRangeException("Input value is out of range.");
        System.out.println("Enter an integer calue between "+MIN+"and"+MAX+",inclusive");

        int value = in.nextInt();
        if(value<MIN||value>MAX){//判断是不是在有效范围内，不在，执行throw语句
            throw e;
        }
        System.out.println("END");//未执行
    }
}
