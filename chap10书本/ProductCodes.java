package com.company;

import java.util.Scanner;

public class ProductCodes {
    public static void main(String[] args) {
        String code;
        char zone;
        int district,valid =0,banned = 0;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter product code (STOP to stop):  ");
        code = scan.nextLine();
        while(!code.equals("STOP")){
            try {//提取出产地和销售地信息，判定是否禁售
                zone = code.charAt(9);
                district = Integer.parseInt(code.substring(3, 7));//将String字符类型数据转换为Integer整型数据，遇到一些不能被转换为整型的字符时，会抛出异常.
                valid++;//没有抛出异常才增加一
                if (zone == 'R' && district > 2000)
                    banned++;
            }
            catch(StringIndexOutOfBoundsException exception){
                System.out.println("Improper code length:  "+code);
            }
            catch(NumberFormatException exception){//substring没有得到一个完整的整数值
                System.out.println("District is not numeric:  "+code);
            }
            System.out.print("Enter product code (STOP to stop)");
            code = scan.nextLine();
        }
        System.out.println("# of valid codes entered:"+valid);
        System.out.println("# of banned codes entered: "+banned);

    }
}
