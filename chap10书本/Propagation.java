package com.company;

public class Propagation {
    public static void main(String[] args) {
        ExceptionScope demo = new ExceptionScope();
        System.out.println("Programme beginning :");
        demo.level1();
        System.out.println("Programme ending.");
    }
}
