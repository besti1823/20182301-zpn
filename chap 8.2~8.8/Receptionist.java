package com.company;

public class Receptionist extends Employee {

    @Override
    public String Do() {
        return "Could you tell me how to get to the foreign department?";
    }

    @Override
    public String tostring() {
        return "I am a receptionist";
    }
}
