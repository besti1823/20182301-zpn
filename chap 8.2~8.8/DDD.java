package com.company;

public class DDD {
    private Geometry[] geometrylist;
    public DDD(){
        geometrylist = new Geometry[3];
        geometrylist[0]= new Zhengfangti("正方体",4);
        geometrylist[1]= new Zhengsanleng("正三棱",3);
        geometrylist[2]= new Yuan("正五棱",5);
    }

    public void DAll(){
        geometrylist[0].area();
        geometrylist[0].perimeter();
        geometrylist[0].volume();
        geometrylist[1].area();
        geometrylist[1].perimeter();
        geometrylist[1].volume();
        geometrylist[2].area();
        geometrylist[2].perimeter();
        geometrylist[2].volume();
    }

}
