package com.company;

public class Nurse extends Employee {
    @Override
    public String Do(){
        return "May I have a thermometer?";
    }

    @Override
    public String tostring() {
        return "I am a nurse";
    }
}
