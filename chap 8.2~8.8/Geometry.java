package com.company;

public abstract class Geometry {

    private String name;
    private double shuju;
    private double perimeter;
    private double area;
    private double volume;

    public Geometry(String name, double shuju) {
        this.name = name;
        this.shuju = shuju;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getShuju() {
        return shuju;
    }

    public void setShuju(double shuju) {
        this.shuju = shuju;
    }




    @Override
    public String toString() {
        return "Geometry{" +
                "name='" + name + '\'' +
                ", shuju=" + shuju +
                ", perimeter=" + perimeter +
                ", area=" + area +
                ", volume=" + volume +
                '}';

    }
    public abstract void perimeter();
    public abstract void area();
    public abstract void volume();
}
