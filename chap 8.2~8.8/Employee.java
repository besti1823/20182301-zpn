package com.company;

public abstract class Employee {
    public String Ask(){
        return "Can I help you ?";
    }
    public abstract String Do();
    public abstract String tostring();
}
