package com.company;

public class Zhengfangti extends Geometry {

public double a;

    public Zhengfangti(String name, double shuju) {
      super(name,shuju);
       a =shuju ;
    }

    @Override
    public void perimeter() {
        double perimeter=12*a;
        System.out.println(perimeter);
    }

    @Override
    public void area() {
        double area=6*a*a;
        System.out.println(area);
    }

    @Override
    public void volume() {
        double volume = a*a*a;
        System.out.println(volume);

    }
}
