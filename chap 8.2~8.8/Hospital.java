package com.company;

public class Hospital {
    public static void main(String[] args) {
        Doctor a = new Doctor();
        Nurse b = new Nurse();
        Executive c = new Executive();//行政人员
        Surgeon d = new Surgeon();//外科医生
        Receptionist e = new Receptionist();//接待员
        Doorman f = new Doorman();//门卫

        System.out.println(a.tostring());
        System.out.println(a.Ask());
        System.out.println(a.Do());

        System.out.println(b.tostring());
        System.out.println(b.Ask());
        System.out.println(b.Do());

        System.out.println(c.tostring());
        System.out.println(c.Ask());
        System.out.println(c.Do());

        System.out.println(d.tostring());
        System.out.println(d.Ask());
        System.out.println(d.Do());

        System.out.println(e.tostring());
        System.out.println(e.Ask());
        System.out.println(e.Do());

        System.out.println(f.tostring());
        System.out.println(f.Ask());
        System.out.println(f.Do());
    }
}