package com.company;

public class Yuan extends Geometry {

    public double a;

    public Yuan(String name, double shuju) {
        super(name,shuju);
        a =shuju ;
    }

    @Override
    public void perimeter() {
        double perimeter=0;
        System.out.println(perimeter);
    }

    @Override
    public void area() {
        double area=4*Math.PI*a*a;
        System.out.println(area);
    }

    @Override
    public void volume() {
        double volume = Math.PI*a*a*a*4/3;
        System.out.println(volume);

    }
}