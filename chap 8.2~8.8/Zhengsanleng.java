package com.company;

public class Zhengsanleng extends Geometry {

    public double a;

    public Zhengsanleng(String name, double shuju) {
        super(name,shuju);
        a =shuju ;
    }

    @Override
    public void perimeter() {
        double perimeter=6*a;
        System.out.println(perimeter);
    }

    @Override
    public void area() {
        double area=Math.sqrt(3)*a*a;
        System.out.println(area);
    }

    @Override
    public void volume() {
        double volume = Math.sqrt(2)*a*a*a/12;
        System.out.println(volume);

    }
}