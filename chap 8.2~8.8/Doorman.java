package com.company;

public class Doorman extends Employee {
    @Override
    public String Do() {
        return "When can I get in?";
    }

    @Override
    public String tostring() {
        return "I am a doorman ";
    }
}
