package com.company;

public class Surgeon extends Employee{
    @Override
    public String Do() {
        return "Will I scar this one?";
    }

    @Override
    public String tostring() {
        return "I am a surgeon ";
    }
}
