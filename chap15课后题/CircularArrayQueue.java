package com.company;

import java.util.Arrays;

public class CircularArrayQueue<T> implements Queue<T> {
    private final int DEFAULT_CAPACITY = 10;
    private int front,rear,count;
    private T[] queue;

    public CircularArrayQueue()
    {
        front = rear = count = 0;//初始化
        queue = (T[]) (new Object[DEFAULT_CAPACITY]);//元素数组
    }
    @Override
    public void enqueue(T element) {
        if(count==queue.length)//如果满了
            expandCapacity();
        queue[rear] =  element;//加入数字
        rear=(rear+1)%queue.length;//队尾指针指向新的队尾
        count++;

    }
    private void expandCapacity() {
        T[] larger = (T[]) (new Object[queue.length * 2]);//类似于行13，扩展到二倍
        for (int index = 0; index < count; index++)
            larger[index] = queue[(front + index) % queue.length];//将原来的队列复制过来
        front = 0;
        rear = count;
        queue = larger;

    }


    @Override
    public T dequeue() throws EmptyCollectionException{//依次弹出队列里的数
        T str;
        if(front == rear&&queue[front]==null)
            throw new EmptyCollectionException("队列已空");
        else {
            str = queue[front];
            queue[front]=null;
            front=(front+1)%queue.length;
            count--;
        }
        return str;
    }

    @Override
    public T first() throws EmptyCollectionException{//弹出队列里的第一个数

        if(front == rear&&queue[front]==null)
        {
            throw new EmptyCollectionException("数组已空");
        }
        else
            return  queue[front];
    }

    @Override
    public boolean isEmpty() {
        if(count == 0)
            return false;
        else
            return true;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        return "CircularArrayQueue{" +
                "DEFAULT_CAPACITY=" + DEFAULT_CAPACITY +
                ", front=" + front +
                ", rear=" + rear +
                ", count=" + count +
                ", queue=" + Arrays.toString(queue) +
                '}';
    }
}
