package com.company;

public class Box{
    private double high;
    private double wide;
    private double longg;
    private boolean full;
    public Box(double h,double w,double l,boolean value){
        high = h;
        wide = w;
        longg = l;
        full = false;//构造函数初始值为flase

    }

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getWide() {
        return wide;
    }

    public void setWide(double wide) {
        this.wide = wide;
    }

    public double getLongg() {
        return longg;
    }

    public void setLongg(double longg) {
        this.longg = longg;
    }

    public boolean isFull() {
        return full;
    }

    public void setFull(boolean full) {
        this.full = full;
    }

    @Override
    public String toString() {
        return "Box{" +
                "high=" + high +
                ", wide=" + wide +
                ", longg=" + longg +
                ", full=" + full +
                '}';
    }
}
