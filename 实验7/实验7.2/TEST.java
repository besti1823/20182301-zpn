package cn.edu.besti.cs1823.Z2301;

public class TEST {
    public static void main(String[] args) {
        Contact[] players = new Contact[3];
        Contact[] player = new Contact[3];



        player[0] = new Contact("d", "j", "2301");
        player[1] = new Contact("e", "k", "0912");
        player[2] = new Contact("f", "l", "0505");

        Contact target1 = new Contact("","","0505");
        Contact target2 = new Contact("","","0000");
        Contact target3 = new Contact("","","2301");
        Contact target4 = new Contact("","","1128");
        Contact target5 = new Contact("","","0910");

        Contact found[] = new Contact[10];
         found[0] = (Contact) Searching.linearSearch(player,target1);
         found[1] = (Contact) Searching.linearSearch(player,target2);
         found[2] = (Contact) Searching.linearSearch(player,target3);
         found[3] = (Contact) Searching.linearSearch(players,target4);
         found[4] = (Contact) Searching.linearSearch(players,target5);
         found[5] = (Contact) Searching.linearSearch(player,target5);
         found[6] = (Contact) Searching.linearSearch(players,target1);
         found[7] = (Contact) Searching.linearSearch(players,target3);

         for(int i=0;i<8;i++){
             System.out.println("Test"+(i+1)+":");
             if(found[i] == null)
                 System.out.println("Player was not found.");
             else
                 System.out.println("Found:  "+ found[i]);
         }

         Sorting.selectionSort(player);
         Sorting.selectionSort(players);

        System.out.println("Test9:");
        for(Comparable play :player)
            System.out.println(play);
        System.out.println("Test10:");
        for(Comparable play :players)
            System.out.println(play);


    }
}
