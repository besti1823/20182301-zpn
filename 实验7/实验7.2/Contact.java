package cn.edu.besti.cs1823.Z2301;



public class Contact implements Comparable
{
    private String firstName, lastName, phone;

    public Contact (String first, String last, String telephone)
    {
        firstName = first;
        lastName = last;
        phone = telephone;
    }

    public String toString ()
    {
        return lastName + ", " + firstName + ":  " + phone;
    }

    public int compareTo (Object other)
    {
        int result;
        result = phone.compareTo(((Contact)other).phone);
        return result;
    }

    public static void select(Contact[] a){
        Contact temp;
        int mix;
        for(int i=0;i<a.length-1;i++)
        {
            mix=i;
            for(int j=i+1;j<a.length;j++)
                if(a[j].phone.compareTo(a[mix].phone)>0)
                    mix=j;
            if(i!=mix)
            {
                temp=a[i];
                a[i]=a[mix];
                a[mix]=temp;
            }
        }
        for(int m=0;m<a.length;m++)
            System.out.println(a[m]);
    }
}


