package com.company;

public class Searching2 {


    //线性查找
    public static int linearSearch(int[] data,int target){
        int result = 0;
        int index = 0;
        while(result == 0 && index < data.length){
            if(data[index]==target)
                result = data[index];
            index++;
        }
        return result;

    }

    //二分查找（折半查找），版本1，要有顺序
    public static int BinarySearch1(int[] data,int target, int n)
    {
        int low, high, mid;
        low = 0;
        high = n-1;
        while(low<=high)
        {
            mid = (low+high)/2;
            if(data[mid]==target)
                return data[mid];
            if(data[mid]>target)
                high = mid-1;
            if(data[mid]<target)
                low = mid+1;
        }
        return -1;
    }


    //插值查找,但是对于该字符串查找并不适用，适用于数据数字查找
    public static int InsertionSearch(int[] a, int x , int left, int right){
        if(x<a[0]||x>a[a.length-1]){	//不加这句会抛出异常，若找的数不在范围内，则mid可能越界//
            	return -1;		}
        int mid = left + (x-a[left])/(a[right]-a[left])*(right-left);
        if(left>right){
            return -1;		}
        if(x<a[mid]){
            return InsertionSearch(a,x,left,mid-1);
        }		else if(x>a[mid]){
            return InsertionSearch(a,x,mid+1,right);
        }		else{
            return a[mid];
        }
    }


            //斐波那契数列，又称黄金分割数列，指的是这样一个数列：1、1、2、3、5、8、13、21、····
            // 该数列越往后相邻的两个数的比值越趋向于黄金比例值（0.618）。n代表几个，key代表要查找的
            public static void fibonacci(int[] f){
                f[0]=0;
                f[1]=1;
                for(int i=2;i<f.length;++i){
                    f[i]=f[i-1]+f[i-2];
                }
            }

    public static int FibonacInsearch(int[] a, int x){

        int left=0, right=a.length-1;
        int k=0;
        int FIB_MAX = 20;
        int[] f = new int[FIB_MAX];
        fibonacci(f);
        while(a.length>f[k]-1){
            k++;
        }
        int[] tmp = new int[f[k]-1];

        System.arraycopy(a,0,tmp,0,a.length);//拷贝a元素到tmp中

        for(int i=a.length;i<f[k]-1;++i){	//right以后的值都相同
            tmp[i]=a[right];
        }
        while(left<=right){
            int mid = left+f[k-1]-1;
            if(x<a[mid]){

                right=mid-1;
                k-=1;
            }
            else if(x>a[mid]){
                left=mid+1;
                k-=2;
            }
            else{
                if(mid<a.length)
                    return a[mid];
                else		//扩展里找到x，返回a的最后一个下标
                    return a.length-1;
            }
        }
        return -1;
    }
            //二叉树查找

            private static Node root;
            private Node temp;
            public Searching2() {
                 root = null;
            }
            public void add(int data1) {//向二叉树插入元素
                if (this.root == null) {
                    root = new Node(data1);
                    temp = root;//存下第一个节点
                } else {
                    addW(root,data1);
                }
            }
            public void addW(Node x,int data1) {//判定插入元素的位置
                //当数据大于等于节点时，放右子树
                if (data1 >= x.getData()) {
                    if (x.getRight() == null) {
                        x.setRight(new Node(data1));
                    } else {
                        addW(x.getRight(),data1);
                    }
                }
                else {//数据小于节点时，放左子树
                    if (x.getLeft() == null) {
                        x.setLeft(new Node(data1));
                    } else {
                        this.addW(x.getLeft(),data1);
                    }
                }
            }
            public void print() {
                root.print();
                System.out.println();
            }
            public Node BSTreesearch(Node x,int data2) {//在二叉树中寻找元素
                if(x==null)
                    return null;
                if(data2==x.getData()){
                    return x;
                }
                else if(data2<x.getData()){
                    return BSTreesearch(x.getLeft(),data2);
                }
                else {
                    return BSTreesearch(x.getRight(),data2);
                }
            }
            public int BS(int target){
                Node k =BSTreesearch(root,target);
                int po = k.getData();
                return po;
            }
            public Node getRoot() {
                return root;
            }

            //分块查找
            public static int blocksearch(int[] index,int[] data,int target,int m){
                int i = Bsearch(index,target);
                System.out.println("在第"+i+"块");
                if(i>=0){
                    int j = m*i;
                    int k = m*(i+1);
                    for(;j<k;j++){
                        if(data[j]==target)
                            return data[j];
                    }
                }
                return -1;
            }

            private static int Bsearch(int[] index, int target) {
                if(index[0]>target){
                    return 0;
                }
                int i =1;
                if((index[i-1]<target)&&(index[i]>target)){
                    return i;
                }
                return -1;
            }

            //哈希查找
            public static int[] addhashlinear(int[] data){
                int[] result=new int[13];
                for(int i=0;i<data.length;i++){
                    int m=data[i]%13;
                    while(result[m]!=0)
                        m++;
                    result[m]=data[i];
                }
                return result;
            }
            public static int hashlinear(int[] data,int target){
                int m = target%13;
                while(data[m]!=target&&m<data.length-1){
                    m++;
                }
                return data[m];
            }
        }
