package com.company;

import cn.edu.besti.cs1823.Z2301.Contact;
import junit.framework.TestCase;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;


public class Searching2Test extends Searching2 {

    @Test
    public void testTestLinearSearch() {
        int[] a = new int[]{1,9,4,3,6,7};
        assertEquals(9,linearSearch(a,9));//边界
        assertEquals(18,linearSearch(a,1));//异常
    }

    @Test
    public void testBinarySearch1() {
        int[] b = new int[]{12,34,56,9,17};
        int[] bb = select(b);
        assertEquals(9,BinarySearch1(bb,9,5));//边界
        assertEquals(17,BinarySearch1(bb,17,5));//正常

    }


    @Test
    public void testInsertionSearch() {
        int[] c = new int[]{40,1,56,34,72};
        int[] cc = select(c);
        assertEquals(56,InsertionSearch(cc,56,0,4));//正常
        assertEquals(17,InsertionSearch(cc,17,0,4));//异常
    }

    @Test
    public void testFibonacciSearch() {
        int[] c = new int[]{40,1,56,34,72};
        int[] cc = select(c);
        assertEquals(34,FibonacInsearch(cc,34));//正常
        assertEquals(72,FibonacInsearch(cc,72));
    }

    @Test
    public void testTestSearch() {
        Node root;
        Node temp;
        int[] number = new int[]{20,18,23,1,9,10};
        Searching2 tree = new Searching2();
        for(int m=0;m<number.length;m++){
            tree.add(number[m]);
        }

        assertEquals(34,tree.BS(34));
        assertEquals(72,tree.BS(72));
    }

    @Test
    public void testTestBlocksearch() {//(int[] index,int[] data,int target,int m)
        int index[]={22,48,86};
        int f[]={22, 12, 13, 8, 9, 20, 33, 42, 44, 38, 24, 48, 60, 58, 74, 49, 86, 53};
        int[] ff = select(f);
        assertEquals(13,blocksearch(index,ff,13,6));//正常
        assertEquals(75,blocksearch(index,ff,75,6));//异常
    }

    @Test
    public void testTestHashlinear() {
        int[] number1 = new int[]{19 ,14 ,23 ,1 ,68, 20, 84 ,27 ,55 ,11 ,10 ,79};
        int[] number2=addhashlinear(number1);
        assertEquals(14,hashlinear(number2,14));//正常
        assertEquals(55,hashlinear(number2,55));
    }

    public static int[] select(int[] a){
        int temp;
        int mix;
        for(int i=0;i<a.length-1;i++) //每次循环数组，找出最小的元素，放在前面，前面的即为排序好的
        {
            mix=i; //假设最小元素的下标
            for(int j=i+1;j<a.length;j++) //将上面假设的最小元素与数组比较，交换出最小的元素的下标
                if(a[j]<a[mix])
                    mix=j;
            //若数组中真的有比假设的元素还小，就交换
            if(i!=mix)
            {
                temp=a[i];
                a[i]=a[mix];
                a[mix]=temp;
            }
        }
        return a;
    }
}