package com.example.select_sort;

public class Node {


    private int data;


    private Node left;


    private Node right;

    public Node(int data) {


        this.data = data;


        left=null;


        right=null;


    }





    public void setData(int data) {


        this.data = data;


    }





    public void setLeft(Node left) {


        this.left = left;


    }





    public void setRight(Node right) {


        this.right = right;


    }





    public int getData() {


        return data;


    }





    public Node getLeft() {


        return left;


    }





    public Node getRight() {


        return right;


    }


    public void print(){


        if(this.left!=null){


            this.left.print();


        }


        System.out.print(data+" ");


        if(this.right!=null){


            this.right.print();


        }



    }


}
