package com.example.select_sort;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import static com.example.select_sort.Select.BinarySearch1;
import static com.example.select_sort.Select.FibonacInsearch;
import static com.example.select_sort.Select.addhashlinear;
import static com.example.select_sort.Select.hashlinear;
import static com.example.select_sort.Select.linearSearch;

public class ShowActivity extends Activity {

    private TextView tv_user6;
    private TextView tv_user7;
    private TextView tv_user8;
    private TextView tv_user9;
    private TextView tv_user10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);

        tv_user6 = (TextView) findViewById(R.id.tv_user6);
        tv_user7 = (TextView) findViewById(R.id.tv_user7);
        tv_user8 = (TextView) findViewById(R.id.tv_user8);
        tv_user9 = (TextView) findViewById(R.id.tv_user9);
        tv_user10 = (TextView) findViewById(R.id.tv_user10);



        Intent intent = getIntent();
        String a = intent.getStringExtra("user");
        String b = intent.getStringExtra("pass");
        int c = Integer.parseInt(b);

        int st[] = new int[a.length()];
        for(int i=0;i<a.length();i++){
            String temp =""+a.charAt(i);
            st[i]=Integer.parseInt(temp);
        }
        String user6 = String.valueOf(linearSearch(st,c));
        Node root;
        Node temp;
        Select tree = new Select();
        for(int m=0;m<st.length;m++){
            tree.add(st[m]);
        }
        String user7 = String.valueOf(Select.BS(c));
        String user8 = String.valueOf(FibonacInsearch(st,c));
        String user9 = String.valueOf(BinarySearch1(st,c,st.length));
        addhashlinear(st);
        String user10 = String.valueOf(hashlinear(st,c));



        tv_user6.setText("线性查找："+user6);
        tv_user7.setText("二叉树查找："+user7);
        tv_user8.setText("斐波那契查找："+user8);
        tv_user9.setText("二分法查找："+user9);
        tv_user10.setText("哈希查找："+user10);
    }
}
