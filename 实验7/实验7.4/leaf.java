package com.company;

public class leaf {
    public leaf right;
    public leaf left;
    public int root;
    public leaf(int a){
        right=null;
        left=null;
        root=a;
    }
    public void dayin(leaf temp){
        if(temp==null){
            return;
        }
        dayin(temp.left);
        System.out.print(temp.root+" ");
        dayin(temp.right);
    }
}
