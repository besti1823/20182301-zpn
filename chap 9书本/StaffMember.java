package com.company;

abstract public class StaffMember {
    protected String name;
    protected String addtress;
    protected String phone;

    public StaffMember(String eName,String eAddtress,String ePhone)
    {
        name = eName;
        addtress= eAddtress;
        phone = ePhone;
    }
    public String toString()
    {
        String result = "Name: "+name +"\n";
        result += "Addtress:  "+ addtress +"\n";
        result += "Phone : "+phone;
        return result;
    }
    public abstract double pay();
}
