package com.company;

public class Employee extends  StaffMember {
    protected String socialSecurityNumber;
    protected double payRate;
    public Employee(String eName,String eAddtress,String ePhone,String socSecNumber,double rate)
    {
        super (eName,eAddtress,ePhone);
        socialSecurityNumber= socSecNumber;
        payRate = rate;
    }

    @Override
    public String toString() {
        String result = super.toString();
              result+=  "\n social  Security  Number='" + socialSecurityNumber ;
              return result;
    }

    @Override
    public double pay() {
        return payRate;
    }
}
