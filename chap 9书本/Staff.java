package com.company;

public class Staff {
    private StaffMember[] stafflist;
    public Staff(){
        stafflist = new StaffMember[6];
        stafflist[0] = new Executive("Tony","123 Main Line","555-0469","123-45-6789",2423.07);
        stafflist[1] = new Employee("Paulie","456 Off Line","555-0101","987-65-4321",1246.15);
        stafflist[2] = new Employee("Paulie","789 Off Rocker","555-0000","010-20-3040",1169.23);
        stafflist[3] = new Hourly("Michael","678 Fifth Ave","555-0690","958-47-3625",10.55);
        stafflist[4] = new Volunteer("Adrianna","987 Babe Bivd","555-8374");
        stafflist[5] = new Volunteer("Paulie","456 Off Line","555-7282");
        ((Executive)stafflist[0]).awardBonus(500.00);
        ((Hourly)stafflist[3]).addHours(40);

    }
    public void payday()
    {
        double amount;
        for(int count = 0;count<stafflist.length;count++)
        {
            System.out.println(stafflist[count]);
            amount = stafflist[count].pay();
            if(amount == 0.0)
                System.out.println("Thanks!");
            else
                System.out.println("Paid:"+ amount);
            System.out.println("--------------------------------------");
        }
    }
}
