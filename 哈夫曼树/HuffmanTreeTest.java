package com.company;


import java.io.*;
import java.util.Scanner;

public class HuffmanTreeTest {
    public static void main(String[] args) throws IOException, EmptyCollectionException {
        File file = new File("E:\\idea\\week_12s\\input.txt");

        Scanner scan = new Scanner(file);//Scanner的新用法
        String s = scan.nextLine();//读取出来字符串
        System.out.println(s);
        int[] array = new int[26];
        for (int i = 0; i < array.length; i++) {
            array[i] = 0;
        }//把每一个进行初始化，概率论为0
        for (int i = 0; i < s.length(); i++) {
            char x = s.charAt(i);
            array[x - 'a']++;
        }//每一次出现增加
        System.out.println("打印各字母出现频率：");
        for (int i = 0; i < array.length; i++) {
            System.out.print((char) ('a' + i) + ":" + (double) array[i] / s.length() + "\n");
        }//把26个字母的概率都打出来

        HuffmanTreeNode[] huffmanTreeNodes = new HuffmanTreeNode[array.length];//构建哈夫曼树的结点
        for (int i = 0; i < array.length; i++) {
            huffmanTreeNodes[i] = new HuffmanTreeNode(array[i], (char) ('a' + i), null, null, null);
        }//为什么每次都要传left、right、parent，但是传的都是空的，现在它们都是独立的

        HuffmanTree huffmanTree = new HuffmanTree(huffmanTreeNodes);//建立出来哈夫曼树

        System.out.println("打印各字母的编码");
        String[] codes = huffmanTree.getEncoding();
        for (int i = 0; i < codes.length; i++) {
            System.out.println((char) ('a' + i) + ":" + codes[i]);
        }

        //进行编码：二进制加法
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            int x = s.charAt(i) - 'a';
            result += codes[x];
        }
        System.out.println("编码结果：" + result);
        //写入文件
        File file1 = new File("E:\\idea\\week_12s\\output.txt");
        FileWriter fileWriter = new FileWriter(file1);
        fileWriter.write(result);
        fileWriter.close();

        //从文件读取
        Scanner scan1 = new Scanner(file1);
        String s1 = scan1.nextLine();
        HuffmanTreeNode huffmanTreeNode = huffmanTree.getRoot();
        //进行解码
        String result2 = "";
        for (int i = 0; i < s1.length(); i++) {
            if (s1.charAt(i) == '0') {
                if (huffmanTreeNode.left != null) {
                    huffmanTreeNode = huffmanTreeNode.left;
                }
            } else {
                if (s1.charAt(i) == '1') {
                    if (huffmanTreeNode.right != null) {
                        huffmanTreeNode = huffmanTreeNode.right;
                    }
                }
            }
            if (huffmanTreeNode.left == null && huffmanTreeNode.right == null) {
                result2 += huffmanTreeNode.element;//把一个个字母加起来
                huffmanTreeNode = huffmanTree.getRoot();//移到上一个结点
            }
        }
        System.out.println("解码结果：" + result2);
        //写入文件
        File file2 = new File("E:\\idea\\week_12s\\put.txt");
        FileWriter fileWriter1 = new FileWriter(file1);
        fileWriter1.write(result2);

        // 判断解码后原来是否一致
        System.out.println("编码解码后原来是否一致：" + result2.equals(s));
    }
}

