package com.company;

public class CountFlips {
    public static void main(String[] args)
    {
        final int PLIPS = 1000;
        int heads = 0,tails = 0;
        Coin myCoin = new Coin();
        for(int count=1;count<=PLIPS;count++)
        {
            myCoin.flip();
            if(myCoin.isHead())
            {
                heads++;
            }
            else
                tails++;
        }
        System.out.println("Numbers of flips:" + PLIPS);
        System.out.println("Numbers of heads:" + heads);
        System.out.println("Numbers of tails:" + tails);

    }
}
