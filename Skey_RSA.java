package com.company;
//使用对象流将密钥保存在文件中，加密所用的公钥和解密所用的私钥分开保存。将公钥对外公布，供其他人加密使用，而把私钥秘密保存，在需要解密时使用。
import java.io.*;
import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.*;

public class Skey_RSA{
    public static void main(String args[]) throws Exception{
        KeyPairGenerator kpg=KeyPairGenerator.getInstance("RSA");//创建密钥对生成器
        kpg.initialize(1024);//初始化密钥生成器
        KeyPair kp=kpg.genKeyPair();//生成密钥对
        PublicKey pbkey=kp.getPublic();//创建公钥和私钥
        PrivateKey prkey=kp.getPrivate();
        //  保存公钥
        FileOutputStream  f1=new FileOutputStream("Skey_RSA_pub.dat");
        ObjectOutputStream b1=new  ObjectOutputStream(f1);
        b1.writeObject(pbkey);
        //  保存私钥
        FileOutputStream  f2=new FileOutputStream("Skey_RSA_priv.dat");
        ObjectOutputStream b2=new  ObjectOutputStream(f2);
        b2.writeObject(prkey);
    }
}