public class StringBufferDemo{
  public static void main(String [] args){
            StringBuffer buffer = new StringBuffer();
            //和 String 类不同的是，
            // StringBuffer 和 StringBuilder 类的对象能够被多次的修改，
            // 并且不产生新的未使用对象。
            buffer.append('S');
            buffer.append("tringBuffer");
  //          System.out.println(buffer.charAt(1));//输出第二个字符
            System.out.println(buffer.capacity());//capacity返回的不是字符串的长度，而是目前的最大容量。
   //         System.out.println(buffer.indexOf("tring12345"));  //返回值是指定的字符在字符串中的绝对位置
            System.out.println(buffer.length());//返回字符串的长度
            System.out.println("buffer = " + buffer.toString());//String toString()返回此序列中数据的字符串表示形式
         }
}