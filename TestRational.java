package com.company;

import java.util.Scanner;

public class TestRational {
    public static void main(String[] args) {
        Scanner scan =new Scanner(System.in);
        System.out.println("Please input 分子num1,num2,分母den1,den2: ");
        int num1= scan.nextInt();
        int num2= scan.nextInt();
        int den1= scan.nextInt();
        int den2= scan.nextInt();
        RationalNumber r1 = new RationalNumber(num1,den1);
        RationalNumber r2  = new RationalNumber(num2,den2);
        RationalNumber r3;
        RationalNumber r4;
        RationalNumber r5;
        RationalNumber r6;
        RationalNumber r7 ;
        System.out.println("First rational number: "+ r1);
        System.out.println("Second rational number:"+ r2);
        if(r1.isLike(r2))
            System.out.println("r1 and r2 are equal.");
        else
            System.out.println("r1 and r2 are not equal.");
        r3 = r1.reciprocal();
        System.out.println("The reciprocal of r1 is:"+r3);
        System.out.println("Please input the symbol:");
        String m = scan.next();
        switch(m) {
            case "+":
                r4 = r1.add(r2);
                System.out.println("r1 +r2:" + r4);
                break;
            case "-":
                r5 = r1.subtract(r2);
                System.out.println("r1 -r2 :" + r5);
                break;
            case "*":
                r6 = r1.multiply(r2);
                System.out.println("r1 *r2 :" + r6);
                break;
            case "/":
                r7 = r1.divide(r2);
                System.out.println("r1 /r2 :" + r7);
                break;
        }
    }

}
