package com.company;

import java.util.LinkedList;
import java.util.Scanner;

public class LinkedBinaryTree<T> {
   public  BTNode root;
   static int count = 0;

   public LinkedBinaryTree() {
      root = null;
   }

   public static BTNode Create(String arr) {


      if ( count >= arr.length() || arr.charAt(count) == '#') {
         count++;// 跳过空结点
         return null;        }
      BTNode node = new BTNode<>(arr.charAt(count++));
      node.left = Create(arr);
      node.right = Create(arr);
      return node;
   }

   public static void preOrder(BTNode root) {
      if (root != null) {
         System.out.print(root.element + " ");
         preOrder(root.left);
         preOrder(root.right);
      }
   }

   public static void levelOrderTraverse(BTNode root) {
      BTNode temp = root;
      LinkedList<BTNode> linkedList = new LinkedList<BTNode>();
      linkedList.add(temp);
      while (!linkedList.isEmpty()) {
         temp = linkedList.poll();
         System.out.println(temp.element + " ");
         if (temp.left != null) {
            linkedList.add(temp.left);
         }
         if (temp.right != null) {
            linkedList.add(temp.right);
         }
      }
   }

   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      System.out.println("请输入一串字符串");
      String str = scan.nextLine();

      System.out.println("下面进行创建树");
      BTNode root = Create(str);
      System.out.println("层次遍历为 ：  ");
      levelOrderTraverse(root);
      System.out.println("前序遍历为 ：  ");
      preOrder(root);

   }


}
