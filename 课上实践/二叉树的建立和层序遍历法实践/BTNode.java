package com.company;

public class BTNode<T>
{
   protected T element;
   protected BTNode<T> left, right;

   //  Creates a new tree node with the specified data.
   public BTNode (T element)
   {
      this.element = element;
      left = right = null;
   }

   //  Creates a tree node with two child trees
   public BTNode (T element,BTNode left,BTNode right)
   {
      this.element=element;
      this.left=left;
      this.right=right;
   }



}
