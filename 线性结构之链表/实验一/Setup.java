package com.company;

import java.util.Arrays;
import java.util.Scanner;


public class Setup {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("请输入一串学号日期时间的整数（eg：20， 17，23，1， 20， 18，10，1，16，23，49）：  ");
        String str = scan.nextLine();
        String[] arr = str.split(","); // 用,分割
        System.out.println(Arrays.toString(arr)); // [0, 1, 2, 3, 4, 5]
        NumNode head = new NumNode(Integer.valueOf(arr[0]));
        int[] intss=new int[20];
        intss=StringToInt(arr);

        for(int i=1;i<10;i++){
            NumNode x = new NumNode(intss[i]);
            AddTrail(head,x);
        }

        Setup.PrintLink(head);
            int nzpn=Setup.length(head);
        System.out.println("\n"+"链表的个数为：  "+nzpn);

    }
    public static int[] StringToInt(String[] arrs){

    int[] ints = new int[arrs.length];

   for(int i=0;i<arrs.length;i++){

   ints[i] = Integer.parseInt(arrs[i]);

   }

     return ints;

    }

    private static class NumNode
    {
        protected NumNode next = null;
        protected int num;
        public NumNode(int num)
        {
            this.num = num;
            next = null;
        }
    }
    public static void AddTrail(NumNode head, NumNode node)
    {
        // 尾插法：在链表的尾部插入节点
        if ( head == null)//如果头部为空，直接放入头部
            head = node;
        else {
            NumNode temp = head;
            while (temp.next != null) {
                temp = temp.next;//头部不为空，下一个
            }
            temp.next = node;//尾部加入node
        }
    }
    public static void PrintLink(NumNode Head) {//打印出来
        NumNode node = Head;
        while (node != null) {
            System.out.print(" " + node.num);//node.num表示数字
            node = node.next;
        }
    }
    public static int length(NumNode head){
        NumNode temp = head;
        int nZhaoPeining=1;
        while(temp.next!=null){
            temp=temp.next;
            nZhaoPeining++;
        }
        return nZhaoPeining;
    }
}
