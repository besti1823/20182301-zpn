package com.example.jllb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.StringTokenizer;

import static com.example.jllb.Size.AddHead;
import static com.example.jllb.Size.AddShu;
import static com.example.jllb.Size.AddTrail;
import static com.example.jllb.Size.Delete;
import static com.example.jllb.Size.PrintLink;
import static com.example.jllb.Size.Select;
import static com.example.jllb.Size.StringToInt;
import static com.example.jllb.Size.length;

public class MainActivity extends AppCompatActivity {

    private TextView et_username;
    private Button btn_send;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_username = (TextView) findViewById(R.id.et_username);
        final String str = String.format(String.valueOf(et_username));
        btn_send = (Button) findViewById(R.id.btn_send);

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this,PrintActivity.class);



                    //从文件读取数字
                /*InputStream input=getResources().openRawResource(R.raw.test);
                BufferedInputStream bufferedInputStream = new BufferedInputStream(input);
                byte[] buffer = new byte[1024];
                String content = "";
                int flag = 0;
                while (true){
                    try {
                        if (!((flag =bufferedInputStream.read(buffer))!=-1)) break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    content += new String(buffer,0,flag);
                }

                    StringTokenizer tokenizer = new StringTokenizer(content);
                    int[] number = new int[tokenizer.countTokens()];
                    int y = 0;
                    while(tokenizer.hasMoreTokens()){
                        number[y] = Integer.parseInt(tokenizer.nextToken());
                        y++;
                    }
                    int m = number[0];
                    Size.NumNode m1 = new Size.NumNode(m);
                    int n = number[1];
                    Size.NumNode n1 = new Size.NumNode(n);*/

                int m = 2;
                Size.NumNode m1 = new Size.NumNode(m);
                int n = 5;
                Size.NumNode n1 = new Size.NumNode(n);


                    String[] arr = str.split(","); // 用,分割
                    System.out.println(Arrays.toString(arr)); // [0, 1, 2, 3, 4, 5]
                    Size.NumNode head = new Size.NumNode(Integer.valueOf(arr[0]));
                    int[] intss=new int[20];
                    intss=StringToInt(arr);

                    for(int i=1;i<10;i++){
                        Size.NumNode x = new Size.NumNode(intss[i]);
                        AddTrail(head,x);
                    }

                    int nzpn=length(head);

                intent.putExtra("username",nzpn);
                intent.putExtra("count", PrintLink(head));

                    //System.out.println("在第五位插入文件中第一个数");
                    AddShu(5,m1,head);
                intent.putExtra("count1", PrintLink(head));

                    //System.out.println("在第0位插入第二个数");
                    head=AddHead(head,n1);
                intent.putExtra("count2", PrintLink(head));

                    //System.out.println("从链表中删除刚才的数字1");
                    Delete(head,m1);
                intent.putExtra("delect", PrintLink(head));

                    Select(head);
                intent.putExtra("select", PrintLink(head));

                startActivity(intent);
            }
        });
    }
}
