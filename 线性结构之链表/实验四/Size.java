package com.example.jllb;

import java.util.Arrays;

public class Size {


    public static int[] StringToInt(String[] arrs){

        int[] ints = new int[arrs.length];

        for(int i=0;i<arrs.length;i++){

            ints[i] = Integer.parseInt(arrs[i]);

        }

        return ints;

    }

    public static class NumNode
    {
        protected NumNode next = null;
        protected int num;
        public NumNode(int num)
        {
            this.num = num;
            next = null;
        }
    }
    public static void AddTrail(NumNode head, NumNode node)
    {
        // 尾插法：在链表的尾部插入节点
        if ( head == null)//如果头部为空，直接放入头部
            head = node;
        else {
            NumNode temp = head;
            while (temp.next != null) {
                temp = temp.next;//头部不为空，下一个
            }
            temp.next = node;//尾部加入node
        }
    }
    public static String PrintLink(NumNode Head) {//打印出来
        NumNode node = Head;
        int[] abc=new int[10];
        for (int i=0;node != null;i++) {
            {abc[i]=node.num;//node.num表示数字
            node = node.next;}
        }
        return (Arrays.toString(abc));
    }



    public static int length(NumNode head){
        NumNode temp = head;
        int nZhaoPeining=1;
        while(temp.next!=null){
            temp=temp.next;
            nZhaoPeining++;
        }
        return nZhaoPeining;
    }
    public static NumNode Delete(NumNode head, NumNode node) {

        NumNode current = head, prev = head;
        if(current.num==node.num){
            head=current.next;}
        while (current != null) {
            if (current.num != node.num) {
                prev = current;
                current = current.next;
            }
            else {
                break;
            }
        }
        prev.next = current.next;
        if (current.num != node.num)
            System.out.println("找不到节点，删除失败。");
        return head;
    }
    public static NumNode AddHead(NumNode head,NumNode node)//头插法为什么有返回值？因为head改了
    {
        //头插法：在头部插入节点
        node.next = head;
        head = node;
        return head;
    }

    public static void AddShu(int x, NumNode element,NumNode head)
    {
        NumNode temp = head;
        if (x == 0)
        {
            element.next = head;
            head = element;
        }
        else {
            for (int y = 1; y < x - 1; y++) {
                temp = temp.next;
            }
            element.next = temp.next;
            temp.next = element;
        }
    }
    public static void  Select(NumNode head)
    {
        NumNode current = head;
        int temp;

        while (current != null)//现在的指针
        {
            NumNode numNode = current.next;//下一个
            while (numNode != null)
            {
                if (numNode.num < current.num)//如果小于
                {
                    temp = current.num;
                    current.num = numNode.num;
                    numNode.num = temp;
                }
                numNode = numNode.next;//下一个
            }
            current = current.next;//下一个检验，是否需要调换，直到结束



        }
    }
}

