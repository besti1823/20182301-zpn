package com.company;

public class Dictionary2 extends Book2 {
    private int definitions ;

    public Dictionary2(int numPages,int numdefinitions) {
        super(numPages);
        definitions = numdefinitions;
    }

    public double computeRatio()
    {
        return definitions/pages;
    }

    public int getDefinitions() {
        return definitions;
    }

    public void setDefinitions(int definitions) {
        this.definitions = definitions;
    }
}
