package com.company;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.lang.*;


/**
 * Created by besti on 2019/9/29.
 */
public class SE {
    public static void main(String[] args) throws IOException {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口
        ServerSocket serverSocket=new ServerSocket(8800);
        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket=serverSocket.accept();
        //3.获得输入流
        InputStream inputStream=socket.getInputStream();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
        //获得输出流
        OutputStream outputStream=socket.getOutputStream();
        PrintWriter printWriter=new PrintWriter(outputStream);
        //4.读取用户输入信息
        String num1=null;
        String num2=null;
        String den1=null;
        String den2=null;

        System.out.println("服务器已经建立......");

        String n=bufferedReader.readLine();
        if(n !=null){
            System.out.println("我是服务器，用户信息为：" + n);
        }

        char[] arr=n.toCharArray();
        for(int i=0;i<arr.length;i++)
        {
            System.out.println("arr[i]"+ arr[i]);
        }
        int k=(int)arr[0]-48;
        int j=(int)arr[2]-48;
        int x=(int)arr[4]-48;
        int y=(int)arr[6]-48;
        System.out.println(k);
        System.out.println(j);
        System.out.println(x);
        System.out.println(y);

        String fu;
        fu = String.valueOf(arr[3]);
        RationalNumber r1 = new RationalNumber(k,j);
        RationalNumber r2  = new RationalNumber(x,y);
        RationalNumber r4;
        RationalNumber r5;
        RationalNumber r6;
        RationalNumber r7;
        //给客户一个响应
        switch(fu) {
            case "+":
                r4 = r1.add(r2);
                System.out.println(r4);
                String reply1 = "r1 +r2:" + r4;
                printWriter.write(reply1);
                printWriter.flush();
                break;
            case "-":
                r5 = r1.subtract(r2);
                String reply2 = "r1 -r2 :" + r5;
                printWriter.write(reply2);
                printWriter.flush();
                break;
            case "*":
                r6 = r1.multiply(r2);
                String reply3 = "r1 *r2 :" + r6;
                printWriter.write(reply3);
                printWriter.flush();
                break;
            case "/":
                r7 = r1.divide(r2);
                String reply4 = "r1 /r2 :" + r7;
                printWriter.write(reply4);
                printWriter.flush();
                break;
        }
        //5.关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}