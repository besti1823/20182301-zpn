package com.company;


import java.util.Arrays;
public class ArrayStack<T> implements StackADT<T>
{
    private final int DEFAULT_CAPACITY = 100;

    private int top;
    private T[] stack;

    /*使用默认容量创造一个空栈*/
    public ArrayStack() {
        top = 0;
        stack = (T[]) (new Object[DEFAULT_CAPACITY]);

    }
    /*使用指定容量创造一个空栈，参数initialCapacity表示的是指定容量*/
    public ArrayStack(int initialCapacity){
        top = 0;
        stack = (T[])(new Object[initialCapacity]);

    }

    //
    public int Size(){//与下一个方法isEmpty相呼应
        return top ;
    }

    //
    public boolean isEmpty(){
        if(Size() == 0) {
            return true;
        }
        else
        {
            return false;
        }
    }

    //
    public void push (T element)
    {
        if (Size() == stack.length)
            expandCapacity();//扩展空间
        stack[top] = element;//入栈
        top++;

    }

    public void expandCapacity(){//具体扩展方法
        stack = Arrays.copyOf(stack, stack.length * 2);

    }

    //pop
    public T pop() throws EmptyCollectionException//异常判断
    {
        if(isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }
        top--;//弹出
        T result = stack[top];
        stack[top] = null;

        return result;

    }


    public T peek()throws EmptyCollectionException//返回栈顶的元素但不移除它
    {
        if(isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }
        return stack[top-1];
    }

    public String toString()
    {
        String line = "";

        for(int i = 0 ; i < top; i++)
        {
            line +=  stack[i]+"";
        }
        return line;
    }
}


