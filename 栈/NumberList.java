package com.company;
//注意class和void的区别，类与方法
import java.util.Scanner;

public class NumberList {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("请输入信息： (整数）");
        int a = scan.nextInt();
        NumNode head = new NumNode(a);//设立为栈顶
        String str;
        do {
            System.out.println("请输入信息： ");
            int b = scan.nextInt();

            NumNode x1 = new NumNode(b);
            AddTrail(head, x1);//增加信息

            System.out.println("请确认你是否要进行再次输入： ");
            str = scan.nextLine();
            str = scan.nextLine();

        } while (str.equalsIgnoreCase("y"));
        com.company.NumberList.NumNode x1 = new NumNode(123);
        com.company.NumberList.NumNode x2 = new NumNode(234);
        com.company.NumberList.NumNode x3 = new NumNode(345);

        System.out.println("尾插123");
        AddTrail(head,x1);
        PrintLink(head);
        System.out.println("头插234");
        head=AddHead(head,x2);
        PrintLink(head);
        System.out.println("从中间插345");
        AddMid(head,x2,x3);
        PrintLink(head);
        System.out.println();

        System.out.println("删除234");
        head=Delete(head,x2);
        PrintLink(head);

        System.out.println("请将链表进行排序：  ");
        Select(head);
        PrintLink(head);
    }

    private static class NumNode
    {
        protected NumNode next = null;
        protected int num;
        public NumNode(int num)
        {
            this.num = num;
            next = null;
        }
    }
    public static void PrintLink(NumNode Head) {//打印出来
        NumNode node = Head;
        while (node != null) {
            System.out.print(" " + node.num);//node.num表示数字
            node = node.next;
        }
    }

    public static void AddTrail(NumNode head, NumNode node)
    {
        // 尾插法：在链表的尾部插入节点
        if ( head == null)//如果头部为空，直接放入头部
            head = node;
        else {
            NumNode temp = head;
            while (temp.next != null) {
                temp = temp.next;//头部不为空，下一个
            }
            temp.next = node;//尾部加入node
        }
    }
    public static NumNode AddHead(NumNode head,NumNode node)//头插法为什么有返回值？因为head改了
    {
        //头插法：在头部插入节点
        node.next = head;
        head = node;
        return head;
    }
    public static void AddMid(NumNode Head, NumNode node1, NumNode node2) {
        NumNode point = Head;
        while ((point.num != node1.num) && (point.next != null)) {
            point = point.next;
        }
        if (point.num == node1.num) {
            node2.next = node1.next;
            point.next = node2;
        }
    }

    public static NumNode Delete(NumNode head, NumNode node) {

        NumNode current = head, prev = head;
        if(current.num==node.num){
            head=current.next;}
        while (current != null) {
            if (current.num != node.num) {
                prev = current;
                current = current.next;
            }
            else {
                break;
            }
        }
        prev.next = current.next;
        if (current.num != node.num)
            System.out.println("找不到节点，删除失败。");
        return head;
    }
    public static void  Select(NumNode head)
    {
        NumNode current = head;
        int temp;

        while (current != null)//现在的指针
        {
            NumNode numNode = current.next;//下一个
            while (numNode != null)
            {
                if (numNode.num < current.num)//如果小于
                {
                    temp = current.num;
                    current.num = numNode.num;
                    numNode.num = temp;
                }
                numNode = numNode.next;//下一个
            }
            current = current.next;//下一个检验，是否需要调换，直到结束
        }
    }

}
