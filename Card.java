package com.company;

public class Card {
    private int num;
    private enum Color{hongtao,heitao,fangpian,meihua}
    private Color color ;

    public Card() //初始化为随机
    {
        this.roll();
    }



    public void roll(){
        num = (int)(Math.random()*13+1);//点数，直接输出数字就好
        switch ((int)(Math.random()*4+1)){
            case 1:
                color = Color.meihua ;
                break;
            case 2:
                color = Color.hongtao;
                break;
            case 3:
                color = Color.heitao;
                break;
            case 4:
                color = Color.fangpian;
                break;
        }


    }

    public String toString() {
        return "Card{" +
                "num=" + num +
                ", color=" + color +
                '}';
    }
}