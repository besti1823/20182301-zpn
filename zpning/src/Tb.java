import java.text.DecimalFormat;
import java.util.*;
public class Tb {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
	    System.out.println("please input the radius of ball:");
	    float a =scan.nextFloat();
	    double area,volume;
	    area = 4*Math.PI*a*a;
	    volume = 4.0/3.0*Math.PI*a*a*a;
        DecimalFormat fmt = new DecimalFormat("0.0000");
        System.out.println("its area: "+fmt.format(area)+"\n"+"its volume: "+fmt.format(volume));
    }
}

