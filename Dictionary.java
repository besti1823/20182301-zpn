package com.company;


    public class Dictionary extends Book
    {
        private int definitions = 52500;
        public double computeRatio()
        {
            return definitions/pages;
        }

        public int getDefinitions() {
            return definitions;
        }

        public void setDefinitions(int definitions) {
            this.definitions = definitions;
        }
    }

