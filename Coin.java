package com.company;

public class Coin {
    private final int HEADS = 0;
    private int face;
    public Coin()
    {
        flip();
    }
    public void flip(){
        face = (int)(Math.random()*2);
    }
    public boolean isHead(){
        return (face==HEADS);
    }
    public String toString()

    {
        return (face==HEADS)?"Heads":"Tails";
    }

}
