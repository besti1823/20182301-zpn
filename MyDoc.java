abstract class Data//抽象类，关键字abstract修饰，不进行实例化，用于扩展延伸
{
    abstract public void DisplayValue();
}

class Integer extends Data //在Data上拓展Integer
{
    int value;
    Integer()//实例化
    {
        value = 333;
    }
    public void DisplayValue()//抽象类方法具体化
    {
        System.out.println(value);
    }
}

class Double extends Data //在Data上拓展Double
{
    double value;
    Double(){
        value = 163.0987654;
    }
    public void DisplayValue(){
        System.out.println(value);
    }
}

abstract class Factory{
    abstract public Data CreateDataObject();
}

class IntFactory extends Factory //在Factory上拓展IntFactory
{
    public Data CreateDataObject(){
        return new Integer();
    }
}

class Document{
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}

class DoubleFactory extends Factory {
    public Data CreateDataObject(){
        return new Double();
    }
}

public class MyDoc {
    static Document i,d;
    public static void main(String[] args) {
//        int a = 20182301%6;
//        System.out.println("学号运算结果为："+a);
//        if (a==5)
//            System.out.println("让系统支持Double类");
        i = new Document(new IntFactory());
        i.DisplayData();
        d = new Document(new DoubleFactory()) ;
        d.DisplayData();
    }
}