package com.company;
//字节流
import java.io.*;

/**
 * Created by besti on 2019/10/12.
 */
public class FileTest1 {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("D:/File","HelloWorld.txt");
                //File file = new File("HelloWorld.txt");
//        File file1 = new File("C:\\Users\\besti\\Desktop\\FileTest\\Test\\Test");
//        file1.mkdir();
//        file1.mkdirs();

//返回值为 boolean； 方法介绍：当且仅当不存在具有此抽象路径名指定名称的文件时，不可分地创建一个新的空文件。==不允许创建文件夹
        if (!file.exists()){
            file.createNewFile();
        }
        // file.delete();
        //（2）文件读写
        //第一种：字节流读写，先写后读
        //output写入
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] hello = {'H','e','l','l','o',',','W','o','r','l','d','!'};
        outputStream1.write(hello);
        outputStream1.flush();//可有可无，不执行任何操作！！！

        //input读入
        InputStream inputStream1 = new FileInputStream(file);
        while (inputStream1.available()> 0){//available()方法接受远方传递
            //注意： 这个方法可以在读写操作前先得知数据流里有多少个字节可以读取需要注意的是，如果这个方法用在从本地文件读取数据时，
            // 一般不会遇到问题，但如果是用于网络操作，就经常会遇到一些麻烦。比如，Socket通讯时，对方明明发来了1000个字节，
            // 但是自己的程序调用available()方法却只得到900，或者100，甚至是0，感觉有点莫名其妙，怎么也找不到原因。其实，
            // 这是因为网络通讯往往是间断性的，一串字节往往分几批进行发送。本地程序调用available()方法有时得到0，
            // 这可能是对方还没有响应，也可能是对方已经响应了，但是数据还没有送达本地。对方发送了1000个字节给你，也许分成3批到达，
            // 这你就要调用3次available()方法才能将数据总数全部得到。
            System.out.print((char) inputStream1.read()+"  ");
        }
        inputStream1.close();
        System.out.println("\n文件读写结束：OutputStream、InputStream先写后读");

    }
}
