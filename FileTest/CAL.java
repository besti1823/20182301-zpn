package com.company;
//这类可以将数字和字符完美结合，以后的计算器可以进行参考
public class CAL {
    private int m;
    private String str,s1,s2;
    public CAL(String a) {
        str = a;
        char[] ch = str.toCharArray();
        for (int i = 0; i < str.length(); i++) {
            if (ch[i] == '+' || ch[i] == '-')
                m = i;
        }//当遇到正号或者负号时，记录数组为第几个
        if (ch[m] == '-') {
            s1 = str.substring(0, m);
            s2 = "-" + str.substring(m + 1);//将前面的符号与数字连起来
        }
        else {
            s1 = str.substring(0, m);
            s2 = str.substring(m + 1);
        }
    }
    public double getreal(){
        return Double.parseDouble(s1);
    }
    public double getimage(){
        return Double.parseDouble(s2);
    }
}