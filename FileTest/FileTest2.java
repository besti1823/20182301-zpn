package com.company;
//字符流
import java.io.*;

public class FileTest2 {
    public static void main(String[] args) throws IOException{
        File file = new File("D:/File","HelloWorld2.txt");
        if (!file.exists()){
            file.createNewFile();
        }
        //============================BufferedInputStream====================================
        //BufferedWriter  缓冲区  存储的数据量默认是1024byte
        byte[] buffer = new byte[1024];
        String content = "";
        int flag = 0;

        Writer writer2 = new FileWriter(file);
        writer2.write("Hello, I/O Operataion!这是利用Writer写入文件的内容");
        writer2.flush();
        writer2.append("Hello,World");
        writer2.flush();

        InputStream inputStream2 = new FileInputStream(file);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream2);

        while ((flag =bufferedInputStream.read(buffer))!=-1){
            content += new String(buffer,0,flag);
        }

        System.out.println(content);
        bufferedInputStream.close();
        System.out.println("文件读结束：BufferedInputStream直接读并输出！");
    }
}
