package com.company;

import java.io.*;
import java.util.Scanner;

public class FileTestZpn {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("D:/File","HelloWorldzpn.txt");
        //File file = new File("HelloWorld.txt");
//        File file1 = new File("C:\\Users\\besti\\Desktop\\FileTest\\Test\\Test");
//        file1.mkdir();
//        file1.mkdirs();

//返回值为 boolean； 方法介绍：当且仅当不存在具有此抽象路径名指定名称的文件时，不可分地创建一个新的空文件。==不允许创建文件夹
        if (!file.exists()){
            file.createNewFile();
        }

        OutputStream zpnoutput = new FileOutputStream(file);
        System.out.println("将两个复数进行写入(正数）：  "+"\n");
        Scanner scan = new Scanner(System.in);
        String a=scan.nextLine();
        zpnoutput.write(a.getBytes());
        zpnoutput.flush();
        String[] reply=a.split("i") ;
        CAL f1=new CAL(reply[0]);
        CAL f2=new CAL(reply[1]);
        double m=f1.getreal();
        double n=f1.getimage();
        double k=f2.getreal();
        double l=f2.getimage();

        //input读入

        InputStream zpninput = new FileInputStream(file);
        System.out.println("将这两个复数读取出来:   ");
        while (zpninput.available()> 0){
            System.out.print((char) zpninput.read()+"  ");

        }

        double c = m+k;
        double d = n+l;

            String answer1="="+c + "+" + d + "i";
            zpnoutput.write( answer1.getBytes());
            zpnoutput.flush();



        zpninput.close();


    }
}
