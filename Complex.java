import java.awt.*;
import java.util.*;

public class Complex {
    private double RealPart;
    private double ImagePart;//定义实部和虚部

    public Complex(double R,double I)//构造函数，接受成员值的传递，并进行保存
    {
        RealPart = R;
        ImagePart = I;
    }

    public double getRealPart()//通过get来进行数据封装
    {
        return RealPart;
    }
    public double getImagePart()
    {
        return ImagePart;
    }

    public Complex ComplexAdd (Complex a)//创一个新的Complex用来保存相加后得到的复数
    {
        double I =ImagePart + a.getImagePart();//根据已有复数创建对象，复制复数a两个成员值。
        double R =RealPart + a.getRealPart();
        return new Complex(R,I);//传递第一个值，改变成员值。
    }
    public Complex ComplexSub (Complex a)
    {
        double I =ImagePart - a.getImagePart();
        double R =RealPart - a.getRealPart();
        return new Complex(R,I);
    }
    public Complex ComplexMulti(Complex a){
        double c = a.getRealPart();
        double d =  a.getImagePart();
        double I = ImagePart * c - RealPart * d;
        double R = RealPart * c + ImagePart * d;
        return new Complex(R,I);
    }
    public Complex ComplexDiv(Complex e){
        double c = e.getRealPart();
        double d =  e.getImagePart();
        double t = c * c + d * d ;
        double R = (RealPart*c + ImagePart*d)/t;
        double I = (ImagePart*c - RealPart*d)/t;
        return new Complex(R,I);
    }
    public boolean Equal(Complex a){
        if(this.equals(a))
            return true;
        else
            return false;
    }
    public String toString(){
        String S = "";
        if(ImagePart>0)
            S = RealPart + "+"+ ImagePart +"i";
        else if(ImagePart<0)
            S = RealPart + "" + ImagePart + "i";
        else
            S = RealPart +"";
        return S;
    }


}
