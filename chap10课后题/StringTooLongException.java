package com.company;

public class StringTooLongException extends Throwable {
    StringTooLongException(String message){
        super(message);
    }
}
