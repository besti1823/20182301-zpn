package com.company;

import java.util.Scanner;

public class CException {
    public static void main(String[] args) throws StringTooLongException{
        final int MAX=20;
        Scanner scan =new Scanner(System.in);
        StringTooLongException e=new StringTooLongException("Input value is too long.");
        System.out.println("Enter a String(less than 20  bytes):  ");

        String value = scan.nextLine();
        int a =value.length();
        System.out.println(a);
        if(a>MAX){//判断是不是在有效范围内，不在，执行throw语句
            throw e;
        }
        System.out.println("END");//未执行

    }
}
