package com.company;

import java.util.Scanner;

public class CException2 {
                public static void main(String[] args) {
                    int MAX =3;
                    Scanner in=new Scanner(System.in);
                    String a=null;
                    do{
                        try{
                            System.out.println("Enter a string (DONE to stop):");
                            a=in.nextLine();
                            if(a.length()>MAX){
                                throw new StringTooLongException("键入过多字符！");
                            }
                        }
                        catch (StringTooLongException e){
                            System.out.println("Too much message!");
                        }
                    }while(!a.equals("DONE"));
                }
            }
